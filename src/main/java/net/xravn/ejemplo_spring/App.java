package net.xravn.ejemplo_spring;

import org.springframework.boot.SpringApplication;

public class App {

	public static void main(String[] args) {
		SpringApplication.run(App.class, args);
	}

}
